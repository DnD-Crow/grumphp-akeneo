#!/usr/bin/env bash

# > Define script variables
GRUMPHP_APP_VERSION='^0.14'
GRUMPHP_DND_APP_VERSION='*'
PHP_CODESNIFFER_APP_VERSION='^3.2'
GRUMPHP_CONFIG_MAIN_PATH='./vendor/agencednd/grumphp-magento-1/utils/templates/grumphp.yml'
GRUMPHP_CONFIG_MAIN_FILENAME='grumphp.yml'
GRUMPHP_CONFIG_PROJECT_PATH='./vendor/agencednd/grumphp-magento-1/utils/templates/grumphp.project.yml'
GRUMPHP_CONFIG_PROJECT_FILENAME='grumphp.project.yml'

# > Vendor must be ignore, avoid accidental committed files from other libraries
function add_vendor_to_gitignore(){
  LOCAL_FILENAME="./.gitignore"
  if [ ! -f ${LOCAL_FILENAME} ]; then
    touch ${LOCAL_FILENAME}
  fi

  grep -q -F '/vendor/' ${LOCAL_FILENAME} || printf '\n/vendor/\n' >> ${LOCAL_FILENAME}
  echo "Successfully add vendor folder to .gitignore."
}

# > Init composer configuration file if not exist
function init_composer(){
  LOCAL_FILENAME="./composer.json"
  if [ ! -f ${LOCAL_FILENAME} ]; then
    touch ${LOCAL_FILENAME}
    echo '{}' >> ${LOCAL_FILENAME}
  fi
  echo "Successfully init composer."
}

# > Add Dn'D Agency packages to composer repository registry
function add_dnd_packages_repository(){
  composer config repositories.agencednd composer https://packages.dnd.fr/ --ansi
}

# > Add GrumpPHP
function add_grumphp_library(){
  composer require --dev phpro/grumphp:${GRUMPHP_APP_VERSION} --ansi
}

# > Add Dn'D Agency specific configuration of GrumPHP for Akeneo
function add_specific_grumphp(){
  composer require --dev agencednd/grumphp-magento-1:${GRUMPHP_DND_APP_VERSION} --ansi
}

# > Add customs libraries needed for our need
function add_libraries(){
  composer require --dev squizlabs/php_codesniffer:${PHP_CODESNIFFER_APP_VERSION} --ansi
  composer require --dev sebastian/phpcpd --ansi
  composer require --dev phpmd/phpmd --ansi
  composer require --dev sensiolabs/security-checker --ansi
}

# > Add GrumPHP configuration with override default if needed.
function add_main_config_file(){
  LOCAL_FILENAME="./"${GRUMPHP_CONFIG_MAIN_FILENAME}
  if [ -e ${LOCAL_FILENAME} ]; then
    echo "rm -rf ${LOCAL_FILENAME}"
  fi

  cp ${GRUMPHP_CONFIG_MAIN_PATH} ${LOCAL_FILENAME}
}

function add_project_config_file(){
  LOCAL_FILENAME="./"${GRUMPHP_CONFIG_PROJECT_FILENAME}
  if [ ! -f ${LOCAL_FILENAME} ]; then
    cp ${GRUMPHP_CONFIG_PROJECT_PATH} ${LOCAL_FILENAME}
  fi
}

add_vendor_to_gitignore
init_composer
#add_dnd_packages_repository
add_grumphp_library
#add_specific_grumphp
add_libraries
add_main_config_file
add_project_config_file

